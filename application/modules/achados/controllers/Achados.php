<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Achados extends MY_Controller{
    public function index(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('inicio');
        $this->load->view('common/footer');
  }

  public function formulario_cadastro(){
    $this->load->view('common/header');
    $this->load->view('common/navbar');
    $this->load->view('form_cadastro');
    $this->load->view('common/footer');
}
}