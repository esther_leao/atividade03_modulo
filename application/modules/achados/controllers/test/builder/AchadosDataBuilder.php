<?php
include_once APPPATH.'controllers/test/builder/TestDataBuilder.php';

class AchadosDataBuilder extends TestDataBuilder {

    public function __construct($table = 'lp2_achados'){
        parent::__construct('itens_achados', $table);
    }

    function getData($index = -1){
        $data[1]['nome'] = 'Aparelho dental';
        $data[1]['local'] = 'Encontrado na pia do banheiro feminino do segundo andar';
        $data[1]['descricao'] = 'Aparelho dental móvel rosa';
        $data[1]['periodo'] = 'Manhã';
        $data[1]['data'] = '25/03/2019';
      

        $data[2]['nome'] = 'Rg';
        $data[2]['local'] = 'Encontrado em frente a portaria, na calçada';
        $data[2]['descricao'] = 'Documento de Rg com nome de Batman';
        $data[2]['periodo'] = 'Noite';
        $data[2]['data'] = '16/06/2019';


        $data[3]['nome'] = 'Laço Infantil';
        $data[3]['local'] = 'Encontrado na Biblioteca';
        $data[3]['descricao'] = 'Laço rosa infantil de colocar no cabelo';
        $data[3]['periodo'] = ' Manhã';
        $data[3]['data'] = '12/01/2019';
        
        $data[4]['nome'] = 'Blusa de frio';
        $data[4]['local'] = 'Encontrado na sala de aula g58';
        $data[4]['descricao'] = 'Blusa de frio cinza e preta com capuz';
        $data[4]['periodo'] = 'Tarde';
        $data[4]['data'] = '02/04/2019';
            
        $data[5]['nome'] = 'Aliança';
        $data[5]['local'] = 'Encontrada no laboratório de engenharia';
        $data[5]['descricao'] = 'Aliança de ouro com nome gravado';
        $data[5]['periodo'] = 'Noite';
        $data[5]['data'] = '21/06/2019';

        $data[6]['nome'] = 'Carteira';
        $data[6]['local'] = 'Encontrada na sala dos professores';
        $data[6]['descricao'] = 'Carteira nova, vazia e sem identificação, com etiqueta de compra';
        $data[6]['periodo'] = 'Tarde';
        $data[6]['data'] = '20/05/2019';

        return $index > -1 ? $data[$index] : $data;
    }

}