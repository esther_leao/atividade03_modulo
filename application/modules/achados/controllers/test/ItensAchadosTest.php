<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/achados/libraries/ItensAchados.php';
include_once APPPATH . 'modules/achados/controllers/test/builder/AchadosDataBuilder.php';

class TarefaTurmaTest extends Toast{
    private $builder;
    private $achados;

    function __construct(){
        parent::__construct('ItensAchadosTest');
    }

    function _pre(){
        $this->builder = new AchadosDataBuilder();
        $this->achados = new ItensAchados();
    }

    
    function test_selecionado_banco_de_teste(){
        $s = $this->builder->database();
        $this->_assert_equals('lp2_achados', $s, 'Erro na seleção do banco de teste');
    }

    function test_insere_registro_na_tabela(){
        $this->builder->clean_table();
        $data = $this->builder->getData(0);
        $id1 = $this->achados->insert($data);
        $this->_assert_equals(1, $id1, "Esperado 1, recebido $id1");


        $task = $this->achados->get(array('id' => 1))[0];
        $this->_assert_equals($data['nome'], $task['nome']);
        $this->_assert_equals($data['local'], $task['local']);
        $this->_assert_equals($data['descricao'], $task['descricao']);
        $this->_assert_equals($data['periodo'], $task['periodo']);
        $this->_assert_equals($data['data'], $task['data']);


        $id2 = $this->achados->insert(array());
        $this->_assert_equals(-1, $id2, "Esperado -1, recebido $id2");

    }

    function test_carrega_todos_os_registros_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        $tasks = $this->tarefa->get();
        $this->_assert_equals(5, sizeof($tasks), "Número de registros incorreto");
    }

    
    function test_atualiza_registro(){
        $this->builder->clean_table();
        $this->builder->build();

        $task1 = $this->tarefa->get(array('id' => 2))[0];
        $this->_assert_equals('12/01/2019', $task1['data'], "Erro na data");
        $this->_assert_equals('manhã', $task1['periodo'], "Erro no periodo");


        $task1['data'] = '12/01/2019';
        $task1['periodo'] = 'tarde';
        $this->tarefa->insert_or_update($task1);


        $task2 = $this->tarefa->get(array('id' => 2))[0];
        $this->_assert_equals($task1['data'], $task2['data'], "Erro na data");
        $this->_assert_equals($task1['periodo'], $task2['periodo'], "Erro no periodo");
    }
    
    function test_remove_registro_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        $task1 = $this->tarefa->get(array('id' => 5))[0];
        $this->_assert_equals('Aliança', $task1['nome'], "Erro no nome");
        $this->_assert_equals('Encontrada no laboratório de engenharia', $task1['local'], "Erro no local");
        $this->_assert_equals('Aliança de ouro com nome gravado', $task1['descricao'], "Erro na descricao");
        $this->_assert_equals('Noite', $task1['periodo'], "Erro no periodo");
        $this->_assert_equals('21/06/2019', $task1['data'], "Erro na data");


        $this->tarefa->delete(array('id' => 5));

        $task2 = $this->tarefa->get(array('id' => 5));
        $this->_assert_equals_strict(0, sizeof($task2), "Registro não foi removido");
    }

}