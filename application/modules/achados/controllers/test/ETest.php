<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/achados/libraries/ItensAchados.php';
include_once APPPATH . 'modules/achados/controllers/test/builder/AchadosDataBuilder.php';

class ETest extends Toast{

    function __construct(){
        parent::__construct('E Test');
    }

    function test_limpa_tabela_de_teste(){
        $builder = new AchadosDataBuilder('lp2_achados');
        $builder->clean_table();

        $achados = new TarefaTurma();
        $data = $achados->get();
        $this->_assert_equals_strict(0, sizeof($data), 'Erro na limpeza da tabela de teste');
    }

}