        <h5>Cadastre o objeto que achou!</h5>
    </div>
</div>
<div class="container mt-3" id="novo_cadastro">
    <div class="card">
    <div class="card-header"><h4>Objeto Encontrado:</h4></div>
        <div class="card-body">
            <form method="POST" class="text-center border border-light p-4">
                <div class="form-row mb-4">
                    <div class="col-md-6">
                        <input type="text" name="nome" value="<?= set_value('nome') ?>" class="form-control" placeholder="Nome">
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="local" value="<?= set_value('local') ?>" class="form-control" placeholder="Local onde foi encontrado">
                    </div>
                </div>
                <div class="form-row mb-4">
                    <div class="col-md-12">
                            <input type="text" name="descricao" value="<?= set_value('descricao') ?>" class="form-control" placeholder="Descrição do objeto encontrado">
                    </div>
                </div>
                <div class="form-row mb-4">
                    <div class="col-md-6">
                        <select class="browser-default custom-select" name="periodo">
                            <option selected>Período que foi encontrado</option>
                            <option value="<?= set_value('manha') ?>">Manhã</option>
                            <option value="<?= set_value('tarde') ?>">Tarde</option>
                            <option value="<?= set_value('noite') ?>">Noite</option>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="data" value="<?= set_value('data') ?>" class="form-control" placeholder="Data">
                    </div>
                </div>
                <div class="text-center text-md-right">
                    <a class="btnupload-form btn btn-primary">Enviar</a>
                </div>
            </form>
        </div>
    </div>
</div>