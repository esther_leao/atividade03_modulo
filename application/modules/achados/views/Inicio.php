    <h5>Veja aqui os itens que foram recentemente achados!</h5>
  </div>
</div>
<!-- Card deck -->
<div class="card-deck">
  <div class="card mb-2">
    <div class="view overlay">
      <img class="card-img-top" src="<?= base_url('imagens/aparelho.jpg')?>" width="350" alt="Aparelho dental">
    </div>
    <div class="card-body">
      <h4 class="card-title">Aparelho dental</h4>
      <p class="card-text">Local: Encontrado na pia do banheiro feminino do segundo andar</p>
      <p class="card-text">Descrição: Aparelho dental móvel rosa</p>
      <p class="card-text">Período: Manhã</p>
      <p class="card-text">Data: 25/03/2019 </p>
    </div>
  </div>

  <div class="card mb-2">
    <div class="view overlay">
      <img class="card-img-top" src="<?= base_url('imagens/rg.jpg')?>" width="350" alt="RG do batman">
    </div>
    <div class="card-body">
      <h4 class="card-title">Rg</h4>
      <p class="card-text">Local: Encontrado em frente a portaria, na calçada</p>
      <p class="card-text">Descrição: Documento de Rg com nome de Batman </p>
      <p class="card-text">Período:Noite</p>
      <p class="card-text">Data: 16/06/2019</p>
    </div>
  </div>


  <div class="card mb-2">
    <div class="view overlay">
      <img class="card-img-top" src="<?= base_url('imagens/laco.jpg')?>" width="350" alt="Laço infantil pra cabelo">
    </div>
    <div class="card-body">
      <h4 class="card-title">Laço Infantil</h4>
      <p class="card-text">Local: Encontrado na Biblioteca</p>
      <p class="card-text">Descrição: Laço rosa infantil de colocar no cabelo</p>
      <p class="card-text">Período: Manhã</p>
      <p class="card-text">Data: 12/01/2019</p>
    </div>
  </div>
</div>

<div class="card-deck">
  <div class="card mb-2">
    <div class="view overlay">
      <img class="card-img-top" src="<?= base_url('imagens/blusa.jpg')?>" width="350" alt="Blusa de Frio">
    </div>
    <div class="card-body">
      <h4 class="card-title">Blusa de frio</h4>
      <p class="card-text">Local: Encontrado na sala de aula g58</p>
      <p class="card-text">Descrição: Blusa de frio cinza e preta com capuz</p>
      <p class="card-text">Período: Tarde</p>
      <p class="card-text">Data: 02/04/2019</p>
    </div>
  </div>

  <div class="card mb-2">
    <div class="view overlay">
      <img class="card-img-top" src="<?= base_url('imagens/alianca.jpg')?>" width="350" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>
    <div class="card-body">
      <h4 class="card-title">Aliança</h4>
      <p class="card-text">Local: Encontrada no laboratório de engenharia</p>
      <p class="card-text">Descrição: Aliança de ouro com nome gravado</p>
      <p class="card-text">Período: Noite</p>
      <p class="card-text">Data: 21/06/2019</p>
    </div>
  </div>


  <div class="card mb-2">
    <div class="view overlay">
      <img class="card-img-top" src="<?= base_url('imagens/carteira.jpg')?>" width="350" alt="Card image cap">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>
    <div class="card-body">
      <h4 class="card-title">Carteira</h4>
      <p class="card-text">Local: Encontrada na sala dos professores</p>
      <p class="card-text">Descrição: Carteira nova, vazia e sem identificação, com etiqueta de compra</p>
      <p class="card-text">Período: Tarde</p>
      <p class="card-text">Data: 20/05/2019</p>
    </div>
  </div>
</div>
<!-- Card deck -->