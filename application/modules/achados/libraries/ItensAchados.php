<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ItensAchados extends Dao {

    function __construct(){
        parent::__construct('itens_achados');
    }

    public function insert($data, $table = null) {
        $cols = array('nome', ' local', 'periodo' , 'descricao', 'data');
        $this->expected_cols($cols);

        return parent::insert($data);
    }
}