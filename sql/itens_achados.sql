-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 23-Jun-2019 às 03:19
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2_achados`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `itens_achados`
--

CREATE TABLE `itens_achados` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `local` varchar(500) NOT NULL,
  `descricao` varchar(500) NOT NULL,
  `periodo` varchar(6) NOT NULL,
  `data` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `itens_achados`
--

INSERT INTO `itens_achados` (`id`, `nome`, `local`, `descricao`, `periodo`, `data`) VALUES
(1, 'Aparelho dental', 'Encontrado na pia do banheiro feminino do segundo andar', 'Aparelho dental móvel rosa', 'Manhã', '25/03/2019'),
(2, 'Rg', 'Encontrado em frente a portaria, na calçada', 'Documento de Rg com nome de Batman', 'Noite', '16/06/2019'),
(3, 'Laço Infantil', 'Encontrado na Biblioteca', 'Laço rosa infantil de colocar no cabelo', 'Manhã', '12/01/2019'),
(4, 'Blusa de frio', 'Encontrado na sala de aula g58', 'Blusa de frio cinza e preta com capuz', 'Tarde', '02/04/2019'),
(5, 'Aliança', 'Encontrada no laboratório de engenharia', 'Aliança de ouro com nome gravado', 'Noite', '21/06/2019'),
(6, 'Carteira', 'Encontrada na sala dos professores', 'Carteira nova, vazia e sem identificação, com etiqueta de compra', 'Tarde', '20/05/2019');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `itens_achados`
--
ALTER TABLE `itens_achados`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `itens_achados`
--
ALTER TABLE `itens_achados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
